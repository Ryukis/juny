﻿using Juny.Com.Api.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Juny.Com.Api
{
    class GlobalHandler
    {
        public delegate void StringDelegate(string path);

        public static event StringDelegate OnFileIOError;

        public static void FileIOError(string path)
        {
            OnFileIOError?.Invoke(path);
        }
        private static void Kernel_OnFileIOError(string path)
        {
            Logger.GetLogger("GLOBAL").Error("(" + path + ") :: FILE NOT FOUND");
        }
    }
}
