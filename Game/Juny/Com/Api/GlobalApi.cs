﻿using Juny.Com.Api.Lang;
using Juny.Com.Api.Loader;
using Juny.Com.Api.Logs;
using Juny.Com.Api.Properties;
using System;
using System.Threading;
using System.Windows.Forms;

namespace Juny.Com.Api
{
    class GlobalApi
    {
        private LangReader lgReader;
        private Log GlobalLogger;

        private UILoader uiLoader;

        public void Start()
        {
            Console.Title = "Juny";
            if (!GlobalConstants.DEBUG) Kernel.ShowWindow(Kernel.FindWindow(null, "Juny"), 0);
            else Console.WriteLine("DEBUG MODE");

            GlobalLogger = Logger.GetLogger("GLOBAL");
            GlobalLogger.Trace("Starting Juny");

            lgReader = new LangReader(Application.CurrentCulture.TwoLetterISOLanguageName);
            uiLoader = new UILoader();

            Thread.Sleep(150);

            Kernel.Gapi.DrawInitialContent();
            Kernel.Gapi.Run();
        }
    }
}
