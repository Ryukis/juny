﻿using Juny.Com.Api.Properties;
using System;
using System.Collections.Generic;
using System.IO;

namespace Juny.Com.Api.Logs
{
    class Log
    {
        public string Name;
        private List<string> logList = new List<string>();

        public Log(string name)
        {
            Name = name;
            if (!Directory.Exists("logs")) Directory.CreateDirectory("logs");
        }
        public void Flush()
        {
            string final = "";
            for(short i = 0;i < logList.Count; i++)
            {
                final += logList[i] + "\n";
            }
            File.AppendAllText("logs/" + Name + "_log.txt", final);
            final = null;
            logList.Clear();
        }
        public void Trace(string value)
        {
            logList.Add("[" + DateTime.Now.ToString("d/M/y H:m.s") + "] {" + Name + "} |TRACE| : " + value);
            if (GlobalConstants.DEBUG)
            {
                Console.WriteLine("[" + Name + "] : " + value);
            }
        }
        public void Error(string value)
        {
            logList.Add("[" + DateTime.Now.ToString("d/M/y H:m.s") + "] {" + Name + "} |ERROR| : " + value);
            if (GlobalConstants.DEBUG)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("["+Name+"] : "+value);
                Console.ResetColor();
            }
        }
    }
}
