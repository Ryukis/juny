﻿using System;
using System.Collections.Generic;

namespace Juny.Com.Api.Logs
{
    class Logger
    {
        private static List<Log> logList = new List<Log>();

        public static Log GetLogger(string name)
        {
            for(byte i = 0;i < logList.Count; i++)
            {
                if (logList[i].Name == name) return logList[i];
            }
            logList.Add(new Log(name));
            return logList[logList.Count-1];
        }
    }
}
