﻿using Juny.Com.Api.Logs;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Juny.Com.Gapi
{
    class GraphicApi
    {
        public delegate void GenericDelegate();

        public event GenericDelegate OnEnterFrame;

        private Log GraphicLogger;
        private bool isRunning = false;
        private NativeWindow nat;

        public void Start()
        {
            nat = new NativeWindow();
            Rectangle screenBounds = Screen.PrimaryScreen.Bounds;
            GraphicLogger = Logger.GetLogger("GAPI");
            nat.Width = screenBounds.Width;
            nat.Height = screenBounds.Height;
            nat.Location = new Point(screenBounds.X, screenBounds.Y);
            nat.FormBorderStyle = FormBorderStyle.None;
            nat.ShowDialog();
        }
        public void DrawInitialContent()
        {
            using (Graphics grp = nat.CreateGraphics())
            {
                grp.FillRectangle(new SolidBrush(Color.Black), new Rectangle(0, 0, nat.Width, nat.Height));
            }
            GraphicLogger.Trace("Graphical Technologie started");
        }
        public void Run()
        {
            if (!isRunning)
            {
                GraphicLogger.Trace("Graphical Technologie -- FRAMES RUNNING");
                isRunning = true;
                ThreadPool.QueueUserWorkItem(new WaitCallback(FrameRaiser), this);
            }
        }
        public void NextFrame()
        {
            if(!isRunning) FrameRaiser(this);
        }
        public void Stop()
        {
            GraphicLogger.Trace("Graphical Technologie -- FRAMES STOPPED");
            isRunning = false;
        }
        private void FrameRaiser(object state)
        {
            OnEnterFrame?.Invoke();
            ThreadPool.UnsafeQueueUserWorkItem(new WaitCallback(GraphicEngine.Draw), nat);

            Thread.Sleep(1000/45);
            if(isRunning) FrameRaiser(state);
        }
    }
}
