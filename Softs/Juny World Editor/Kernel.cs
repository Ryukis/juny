﻿using Juny.Com.Api;
using Juny.Com.Gapi;
using Juny.Com.Napi;
using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace Juny
{
    class Kernel
    {
        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        private static Thread T_Gapi;
        public static GraphicApi Gapi;

        private static Thread T_Api;
        public static GlobalApi Api;

        private static Thread T_Napi;
        public static NetApi Napi;

        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(true);

            Api = new GlobalApi();
            T_Api = new Thread(Api.Start);
            T_Api.Start();

            Gapi = new GraphicApi();
            T_Gapi = new Thread(Gapi.Start);
            T_Gapi.Start();

            Napi = new NetApi();
            T_Napi = new Thread(Napi.Start);
            T_Napi.Start();
        }
    }
}
