﻿using Juny.Com.Napi.PacketApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Juny.Com.Api.Context
{
    interface IContext
    {
        bool PacketIsValid(Packets packetId);
    }
}
