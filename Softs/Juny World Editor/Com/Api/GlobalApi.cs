﻿using Juny.Com.Api.Logs;
using Juny.Com.Api.Properties;
using Juny.Com.Api.Context;
using System;
using System.Threading;
using Juny.Com.Api.Internal;

namespace Juny.Com.Api
{
    class GlobalApi
    {
        private Log GlobalLogger;
        public IContext Context;

        public void Start()
        {
            Console.Title = "Juny";
            if (!GlobalConstants.DEBUG) Kernel.ShowWindow(Kernel.FindWindow(null, "Juny"), 0);
            else Console.WriteLine("DEBUG MODE");

            GlobalLogger = Logger.GetLogger("GLOBAL");
            GlobalLogger.Trace("Starting Juny World Editor");

            Config.Load();
            Thread.Sleep(150);
            
            Kernel.Gapi.DrawInitialContent();
            Kernel.Gapi.Run();
        }
    }
}
