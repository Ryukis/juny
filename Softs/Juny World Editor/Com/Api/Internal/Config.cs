﻿using Juny.Com.Api.Context;
using Juny.Com.Api.Utils;
using System.IO;
using System.Windows.Forms;

namespace Juny.Com.Api.Internal
{
    class Config
    {
        public static int AuthId;
        public static string AuthName;
        public static string AuthPass;

        public static string Host = "127.0.0.1";
        public static int Port = 50555;

        public static void Load()
        {
            if (File.Exists(Application.CommonAppDataPath + "\\config"))
            {
                BytesReader configReader = new BytesReader(File.ReadAllBytes(Application.CommonAppDataPath + "\\config"));
                AuthId = configReader.ReadInt();
                if(AuthId > 0)
                {
                    AuthName = configReader.ReadUTF();
                    AuthPass = configReader.ReadUTF();
                }
            }
            Kernel.Api.Context = new Authentification();
        }
    }
}
