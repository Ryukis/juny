﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Juny.Com.Api.Utils
{
    class BytesWriter
    {
        public int Position;
        public int BufferSize;
        public byte[] Bytes;

        public void Clear()
        {
            Array.Clear(Bytes, 0, BufferSize);
            Bytes = null;
            BufferSize = 0;
            Position = 0;
        }
        public void Prepare()
        {
            if (Bytes != null)
            {
                if (Bytes.Length > BufferSize)
                {
                    Array.Resize(ref Bytes, BufferSize);
                }
            }
            else
            {
                Bytes = new byte[512];
            }
        }
        private void CheckBufferSize(short newSize)
        {
            if (Bytes == null) Bytes = new byte[newSize];
            BufferSize = newSize;
            if (Bytes.Length < newSize)
            {
                Array.Resize(ref Bytes, newSize);
            }
        }
        public BytesWriter WriteUTFArray(string[] values)
        {
            WriteShort((short)(values.Length));

            for (short i = 0; i < values.Length; i++)
            {
                WriteUTF(values[i]);
            }

            return this;
        }
        public BytesWriter WriteUTF(string value)
        {
            byte[] bstr = ASCIIEncoding.UTF8.GetBytes(value);
            CheckBufferSize((short)(Position + 2 + bstr.Length));
            byte[] shortArray = BitConverter.GetBytes((short)(bstr.Length));
            Array.Reverse(shortArray);

            Buffer.BlockCopy(shortArray, 0, Bytes, Position, 2);
            Position += 2;

            for (short i = 0; i < bstr.Length; i++)
            {
                Bytes[Position] = bstr[i];
                Position++;
            }

            return this;
        }
        public BytesWriter WriteLongArray(long[] values)
        {
            CheckBufferSize((short)(Position + 2 + (values.Length * 8)));
            byte[] shortArray = BitConverter.GetBytes((short)(values.Length));
            Array.Reverse(shortArray);

            for (short i = 0; i < values.Length; i++)
            {
                shortArray = BitConverter.GetBytes(values[i]);
                Array.Reverse(shortArray);

                Buffer.BlockCopy(shortArray, 0, Bytes, Position, 8);
                Position += 8;
            }

            return this;
        }
        public BytesWriter WriteFloat(float value)
        {
            CheckBufferSize((short)(Position + 4));
            byte[] floatValue = BitConverter.GetBytes(value);
            Array.Reverse(floatValue);

            Buffer.BlockCopy(floatValue, 0, Bytes, Position, 4);
            Position += 4;
            return this;
        }
        public BytesWriter WriteDouble(double value)
        {
            CheckBufferSize((short)(Position + 8));
            byte[] doubleArray = BitConverter.GetBytes(value);
            Array.Reverse(doubleArray);
            Buffer.BlockCopy(doubleArray, 0, Bytes, Position, 8);
            Position += 8;

            return this;
        }
        public BytesWriter WriteLong(long value)
        {
            CheckBufferSize((short)(Position + 8));
            byte[] longArray = BitConverter.GetBytes(value);
            Array.Reverse(longArray);

            Buffer.BlockCopy(longArray, 0, Bytes, Position, 8);
            Position += 8;

            return this;
        }
        public BytesWriter WriteIntArray(int[] values)
        {
            CheckBufferSize((short)(Position + 2 + (values.Length * 4)));
            byte[] shortArray = BitConverter.GetBytes((short)(values.Length));
            Array.Reverse(shortArray);

            Buffer.BlockCopy(shortArray, 0, Bytes, Position, 2);
            Position += 2;
            byte[] intArray = new byte[4];

            for (short i = 0; i < values.Length; i++)
            {
                intArray = BitConverter.GetBytes(values[i]);
                Array.Reverse(intArray);

                Buffer.BlockCopy(intArray, 0, Bytes, Position, 4);
                Position += 4;
            }
            Array.Clear(intArray, 0, 4);
            intArray = null;
            return this;
        }
        public BytesWriter WriteInt(int value)
        {
            CheckBufferSize((short)(Position + 4));
            byte[] intArray = BitConverter.GetBytes(value);
            Array.Reverse(intArray);

            Buffer.BlockCopy(intArray, 0, Bytes, Position, 4);
            Position += 4;
            return this;
        }
        public BytesWriter WriteShortArray(short[] values)
        {
            CheckBufferSize((short)(Position + 2 + (values.Length * 2)));
            byte[] shortArray = BitConverter.GetBytes((short)(values.Length));
            Array.Reverse(shortArray);

            Buffer.BlockCopy(shortArray, 0, Bytes, Position, 2);
            Position += 2;

            for (short i = 0; i < values.Length; i++)
            {
                shortArray = BitConverter.GetBytes(values[i]);
                Array.Reverse(shortArray);

                Buffer.BlockCopy(shortArray, 0, Bytes, Position, 2);
                Position += 2;
            }
            Array.Clear(shortArray, 0, 2);
            shortArray = null;

            return this;
        }
        public BytesWriter WriteShort(short value)
        {
            CheckBufferSize((short)(Position + 2));
            byte[] shortArray = BitConverter.GetBytes(value);
            Array.Reverse(shortArray);

            Buffer.BlockCopy(shortArray, 0, Bytes, Position, 2);
            Position += 2;
            return this;
        }
        public BytesWriter WriteByteArray(byte[] values)
        {
            CheckBufferSize((short)(Position + 2 + values.Length));
            byte[] shortArray = BitConverter.GetBytes((short)(values.Length));
            Array.Reverse(shortArray);

            Buffer.BlockCopy(shortArray, 0, Bytes, Position, 2);
            Position += 2;

            for (short i = 0; i < values.Length; i++)
            {
                Bytes[Position] = values[i];
                Position++;
            }

            return this;
        }
        public BytesWriter WriteBytes(byte[] values)
        {
            CheckBufferSize((short)(Position + values.Length));
            Buffer.BlockCopy(values, 0, Bytes, Position, values.Length);
            Position += values.Length;

            return this;
        }
        public BytesWriter WriteByte(byte value)
        {
            CheckBufferSize((short)(Position + 1));
            Bytes[Position] = value;
            Position++;

            return this;
        }
        public string ToHexString()
        {
            string final = "";
            for (short i = 0; i < BufferSize; i++)
            {
                final += Bytes[i].ToString("X") + " ";
            }
            return final.Substring(0, final.Length - 1);
        }
    }
}
