﻿using Juny.Com.Api.Logs;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Juny.Com.Gapi
{
    class GraphicApi
    {
        public delegate void GenericDelegate();

        public event GenericDelegate OnEnterFrame;

        private event GenericDelegate InitialContentDrawCall;

        private Log GraphicLogger;
        private bool isRunning = false;
        private NativeWindow nat;
        private const int FRAME_PER_SECONDS = 1000 / 45;
        public Rectangle ScreenBounds;

        public void Start()
        {
            nat = new NativeWindow();
            ScreenBounds = Screen.GetWorkingArea(nat);
            GraphicLogger = Logger.GetLogger("GAPI");
            nat.FormBorderStyle = FormBorderStyle.None;
            nat.Width = ScreenBounds.Width;
            nat.Height = ScreenBounds.Height;
            nat.Location = new Point(ScreenBounds.X-1, ScreenBounds.Y-1);

            InitialContentDrawCall += OnDrawInitialContentCalled;

            nat.ShowDialog();
        }
        private void OnDrawInitialContentCalled()
        {
            using (Graphics grp = nat.CreateGraphics())
            {
                grp.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                grp.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
                grp.FillRectangle(new SolidBrush(Color.FromArgb(43, 43, 43)), new Rectangle(0, 0, nat.Width, nat.Height));
                Brush WhiteBrush = new SolidBrush(Color.White);
                Pen penLine = new Pen(Color.FromArgb(128, 128, 128), 1);
                Pen penBigLine = new Pen(Color.FromArgb(43, 158, 217), 3);
                grp.DrawString("Juny World Editor", new Font("Caviar Dreams", 18, GraphicsUnit.World), WhiteBrush, new PointF(5, 5));
                grp.DrawLine(penLine, new Point(2, 35), new Point(ScreenBounds.Width - 2, 35));
                grp.DrawLine(penBigLine, new Point(0, 90), new Point(ScreenBounds.Width, 90));
            }
            GraphicLogger.Trace("Graphical Technologie started");
        }
        public void DrawInitialContent()
        {
            InitialContentDrawCall?.Invoke();
        }
        public void Run()
        {
            if (!isRunning)
            {
                GraphicLogger.Trace("Graphical Technologie -- FRAMES RUNNING");
                isRunning = true;
                ThreadPool.QueueUserWorkItem(new WaitCallback(FrameRaiser), this);
            }
        }
        public void NextFrame()
        {
            if(!isRunning) FrameRaiser(this);
        }
        public void Stop()
        {
            GraphicLogger.Trace("Graphical Technologie -- FRAMES STOPPED");
            isRunning = false;
        }
        private void FrameRaiser(object state)
        {
            OnEnterFrame?.Invoke();
            ThreadPool.QueueUserWorkItem(new WaitCallback(GraphicEngine.Draw), nat);

            Thread.Sleep(FRAME_PER_SECONDS*2);
            if(isRunning) FrameRaiser(state);
        }
    }
}
