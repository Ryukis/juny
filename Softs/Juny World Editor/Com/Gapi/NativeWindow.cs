﻿using System.Windows.Forms;

namespace Juny.Com.Gapi
{
    class NativeWindow : Form
    {
        public NativeWindow()
        {
            SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, false);
        }
    }
}
