﻿

using System;

namespace Juny.Com.Napi.Data
{
    class Key
    {
        public byte[] Bytes = new byte[2];
        public string Salt;

        public double EncodePacketId(PacketApi.Packets value)
        {
            return ((double)((double)((short)(value) * Bytes[0]) / Bytes[1]));
        }
        public PacketApi.Packets DecodePacketId(double value)
        {
            return (PacketApi.Packets)(value * Bytes[1] / Bytes[0]);
        }
    }
}
