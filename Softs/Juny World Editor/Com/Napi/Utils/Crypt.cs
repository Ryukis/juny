﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace Juny.Com.Napi.Utils
{
    class Crypt
    {
        public static string EncryptPass(string value, string salt)
        {
            return BCrypt.Net.BCrypt.HashPassword(value, "$2a$10$" + salt).Substring(("$2a$10$"+salt).Length);
        }
    }
}
