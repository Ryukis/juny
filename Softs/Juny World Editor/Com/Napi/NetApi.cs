﻿using Juny.Com.Api.Logs;
using Juny.Com.Api.Utils;
using Juny.Com.Napi.Properties;
using System;
using System.Net;
using System.Net.Sockets;
using Juny.Com.Napi.PacketApi;
using Juny.Com.Napi.Data;
using Juny.Com.Api.Internal;
using Juny.Com.Napi.PacketApi.Process;
using System.Threading;
using System.Text;

namespace Juny.Com.Napi
{
    class NetApi
    {
        public delegate void GenericDelegate(NetApi inst);
        public delegate void PacketDelegate(IPacket packet);

        public event GenericDelegate OnConnect;
        public event GenericDelegate OnIOError;
        public event GenericDelegate OnClose;
        public event PacketDelegate OnPacketReceived;

        public Log NetLogger;
        public NetData Data = new NetData();
        private Socket conn;
        private NetState state;
        public IPacket BufferingPacket = new KeyBuffer();

        private int tmpPacketSize;

        public void Start()
        {
            NetLogger = Logger.GetLogger("NETWORK");
        }
        public void ConnectToServer()
        {
            conn = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            conn.BeginConnect(new IPEndPoint(IPAddress.Parse(Config.Host), Config.Port), OnConnected, this);

            NetLogger.Trace("Connecting to server");
        }
        private void OnConnected(IAsyncResult iar)
        {
            if (!conn.Connected)
            {
                OnIOError?.Invoke(this);
                return;
            }
            prepareReceive();
        }

        #region SendData
        private void SendCallBack(IAsyncResult iar) { }

        public void Send(BytesWriter buffer)
        {
            try
            {
                NetLogger.Trace(">> " + buffer.BufferSize + " bytes [" + buffer.ToHexString() + "]");
                conn.BeginSend(buffer.Bytes, 0, buffer.BufferSize, 0, new AsyncCallback(SendCallBack), conn);
            }
            catch (Exception) { Close(); }
        }
        #endregion

        private void Close()
        {
            OnClose?.Invoke(this);
            try
            {
                conn.Close();
                conn = null;
                NetLogger.Error("Disconnected from server");
            }
            catch (Exception) { }
        }

        #region ReceiveData
        private void prepareReceive()
        {
            Thread.Sleep(1000);
            try
            {
                byte[] data;
                switch (state)
                {
                    case NetState.NEW:
                        data = new byte[26];
                        conn.BeginReceive(data, 0, 26, 0, OnKeyBuffer, data);
                        break;
                    case NetState.WAITING_PACKET_TYPE:
                        data = new byte[8];
                        conn.BeginReceive(data, 0, 8, 0, OnPacketType, data);
                        break;
                    case NetState.WAITING_SMALL_PACKET:
                        data = new byte[2];
                        conn.BeginReceive(data, 0, 2, 0, OnSmallPacketSize, data);
                        break;
                    case NetState.WAITING_MEDIUM_PACKET:
                        data = new byte[4];
                        conn.BeginReceive(data, 0, 4, 0, OnMediumPacketSize, data);
                        break;
                    case NetState.WAITING_FULL_PACKET:
                        data = new byte[tmpPacketSize];
                        conn.BeginReceive(data, 0, tmpPacketSize, 0, OnFullPacket, data);
                        break;
                }
            }
            catch (Exception)
            {
                Close();
            }
        }
        private void OnFullPacket(IAsyncResult iar)
        {
            byte[] data = (byte[])iar.AsyncState;
            BufferingPacket.Reader = new BytesReader(data);
            NetLogger.Trace("<< " + BufferingPacket.PacketId + " :: " + BufferingPacket.Reader.Bytes.Length + " bytes received");
            Data.PacketKey.Bytes[0] = data[data.Length - 2];
            Data.PacketKey.Bytes[1] = data[data.Length - 1];

            OnPacketReceived?.Invoke(BufferingPacket);
            state = NetState.WAITING_PACKET_TYPE;
            prepareReceive();
        }
        private void OnMediumPacketSize(IAsyncResult iar)
        {
            try
            {
                using (BytesReader reader = new BytesReader((byte[])iar.AsyncState))
                {
                    tmpPacketSize = reader.ReadInt();
                    state = NetState.WAITING_FULL_PACKET;
                }
                prepareReceive();
            }
            catch (Exception) { Close(); }
        }
        private void OnSmallPacketSize(IAsyncResult iar)
        {
            try
            {
                byte[] result = (byte[])iar.AsyncState;
                Array.Reverse(result);
                tmpPacketSize = BitConverter.ToInt16(result, 0);
                state = NetState.WAITING_FULL_PACKET;
                prepareReceive();
            }
            catch (Exception) { Close(); }
        }
        private void OnPacketType(IAsyncResult iar)
        {
            try
            {
                using (BytesReader reader = new BytesReader((byte[])iar.AsyncState))
                {
                    BufferingPacket = PacketsCollection.GetPacketById(Data.PacketKey.DecodePacketId(reader.ReadDouble()));
                    if(BufferingPacket == null)
                    {
                        Close();
                        return;
                    }
                    if((short)BufferingPacket.PacketId > 1000)
                    {
                        state = NetState.WAITING_MEDIUM_PACKET;
                    }
                    else
                    {
                        state = NetState.WAITING_SMALL_PACKET;
                    }
                    prepareReceive();
                }
            }
            catch (Exception) { Close(); }
        }
        private void OnKeyBuffer(IAsyncResult iar)
        {
            if(BufferingPacket == null)
            {
                Close();
                return;
            }
            Data.PacketKey = new Key();
            using (BytesReader reader = new BytesReader((byte[])iar.AsyncState))
            {
                Data.PacketKey.Bytes[0] = reader.ReadByte();
                Data.PacketKey.Bytes[1] = reader.ReadByte();
            }

            BytesWriter writer = new BytesWriter();
            writer.WriteByte(Data.PacketKey.Bytes[1]);
            writer.WriteByte(Data.PacketKey.Bytes[0]);
            writer.WriteShort((short)(Encoding.UTF8.GetBytes(Api.Context.Authentification.Username).Length+3));
            writer.WriteByte(1);
            writer.WriteUTF(Api.Context.Authentification.Username);

            state = NetState.WAITING_PACKET_TYPE;
            BufferingPacket = null;
            prepareReceive();

            Send(writer);
            OnConnect?.Invoke(this);
        }
        #endregion
    }
}
