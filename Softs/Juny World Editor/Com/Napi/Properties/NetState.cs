﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Juny.Com.Napi.Properties
{
    enum NetState
    {
        NEW = 0,
        WAITING_PACKET_TYPE = 1,
        WAITING_SMALL_PACKET = 2,
        WAITING_MEDIUM_PACKET = 3,
        WAITING_FULL_PACKET = 4
    }
}
