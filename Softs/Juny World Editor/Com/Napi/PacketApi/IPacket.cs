﻿using Juny.Com.Api.Utils;

namespace Juny.Com.Napi.PacketApi
{
    interface IPacket
    {
        bool Decode();
        BytesWriter Encode();

        Packets PacketId
        {
            get;
        }
        bool CanDestroy
        {
            get;
        }
        bool HasData
        {
            get;
        }
        BytesWriter Writer
        {
            get;
            set;
        }
        BytesReader Reader
        {
            get;
            set;
        }
    }
}
