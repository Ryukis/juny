﻿using Juny.Com.Api.Logs;
using Juny.Com.Napi.PacketApi.Process;
using System;
using System.Collections.Generic;

namespace Juny.Com.Napi.PacketApi
{
    public enum Packets : short
    {
        KEY = 0,
        AUTHENTIFICATION = 895,
        CHARACTERS_COUNT_BY_SERVER = 71
    }
    class PacketsCollection
    {
        private static Dictionary<Packets, Type> packetTypesList = new Dictionary<Packets, Type>()
        {
            { Packets.KEY, typeof(KeyBuffer) },
            {Packets.AUTHENTIFICATION, typeof(Authentification) }
        };
        public static IPacket GetPacketById(Packets id)
        {
            if (packetTypesList.ContainsKey(id))
            {
                Logger.GetLogger("NETWORK").Trace("Packet Id " + Enum.GetName(typeof(Packets), id));
                return (IPacket)(Activator.CreateInstance(packetTypesList[id]));
            }
            Logger.GetLogger("NETWORK").Error("Unexpected packet Id " + id);
            return null;
        }
    }
}
