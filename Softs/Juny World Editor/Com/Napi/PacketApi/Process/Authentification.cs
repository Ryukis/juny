﻿using Juny.Com.Api.Utils;
using Juny.Com.Napi.Utils;
using System;

namespace Juny.Com.Napi.PacketApi.Process
{
    enum AccountValidation : byte
    {
        VALID = 0,
        BANNED = 1,
        INVALID = 2,
        UNAUTHORISED = 3,
        VALID_USERNAME = 4
    }
    class Authentification : IPacket
    {
        private bool hasData = false;
        private BytesReader reader;
        private BytesWriter writer;
        private bool canDestroy = false;

        public BytesWriter Encode()
        {
            writer = new BytesWriter();
            return writer;
        }
        public bool Decode()
        {
                AccountValidation result = (AccountValidation)reader.ReadByte();
                switch (result)
                {
                    case AccountValidation.VALID_USERNAME:
                        Kernel.Napi.Data.PacketKey.Salt = reader.ReadUTF();
                        string cryptResult = Crypt.EncryptPass(Api.Context.Authentification.Pass, Kernel.Napi.Data.PacketKey.Salt);
                        BytesWriter writer = new BytesWriter();
                        writer.WriteByte(0);
                        writer.WriteUTF(cryptResult);
                        Kernel.Napi.Send(new BytesWriter().WriteShort((short)writer.Bytes.Length));
                        Kernel.Napi.Send(writer);
                        writer = null;
                        canDestroy = true;
                        break;
                    case AccountValidation.VALID:
                        canDestroy = true;
                        Kernel.Napi.NetLogger.Trace("Nice account !");
                        break;
                    case AccountValidation.BANNED:
                        Kernel.Napi.NetLogger.Trace("Banned account");
                        break;
                    case AccountValidation.UNAUTHORISED:
                        Kernel.Napi.NetLogger.Trace("Unauthorised account");
                        break;
                    case AccountValidation.INVALID:
                        Kernel.Napi.NetLogger.Error("Error account");
                        break;
                }
                return true;
        }
        public bool CanDestroy
        {
            get { return canDestroy; }
        }
        public Packets PacketId
        {
            get { return Packets.AUTHENTIFICATION; }
        }
        public BytesWriter Writer
        {
            get { return Writer; }
            set { writer = value; }
        }
        public BytesReader Reader
        {
            get { return reader; }
            set { reader = value; }
        }
        public bool HasData
        {
            get { return hasData; }
        }
    }
}
