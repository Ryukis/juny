﻿using Juny.Com.Api.Utils;
using System;
using System.Collections.Generic;

namespace Juny.Com.Napi.PacketApi.Process
{
    class KeyBuffer : IPacket
    {
        private bool hasData = false;
        private BytesReader reader;
        private BytesWriter writer;
        private bool canDestroy = false;

        public BytesWriter Encode()
        {
            writer = new BytesWriter();
            return writer;
        }
        public bool Decode()
        {
            return false;
        }
        public bool CanDestroy
        {
            get { return canDestroy; }
        }
        public Packets PacketId
        {
            get { return Packets.KEY; }
        }
        public BytesWriter Writer
        {
            get { return Writer; }
            set { writer = value; }
        }
        public BytesReader Reader
        {
            get { return reader; }
            set { reader = value; }
        }
        public bool HasData
        {
            get { return hasData; }
        }
    }
}
