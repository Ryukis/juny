﻿using System;
using System.Collections.Generic;

namespace EdgeCore.Com.Ryukis.Net.Game.Data
{
    class Account
    {
        public int Guid;
        public string UserName;
        public byte Gender;
        public byte Rank;
        public string Pass;
        public short BannedId;
        public DateTime LastConnectionDate;
        public DateTime Birthday;
        public DateTime CreationDate;
        public string CreationIp;
        public string LastIp;
        public KeyValuePair<byte, int[]>[] Characters;
        public int[] InterserverGifts;
        public bool IsConnected = false;

        public bool hasCharactersOnServer(byte serverId)
        {
            for(byte i = 0;i < Characters.Length; i++)
            {
                if (Characters[i].Key == serverId && Characters[i].Value.Length > 0) return true;
            }
            return false;
        }
        public byte getCharactersCountOnServer(byte serverId)
        {
            for(byte i = 0;i < Characters.Length; i++)
            {
                if (Characters[i].Key == serverId) return (byte)Characters[i].Value.Length;
            }
            return 0;
        }
    }
}
