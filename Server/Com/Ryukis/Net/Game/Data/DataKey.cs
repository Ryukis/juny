﻿using EdgeCore.Com.Ryukis.Managers;
using EdgeCore.Com.Ryukis.Net.Game.PacketApi;

namespace EdgeCore.Com.Ryukis.Net.Game.Data
{
    class DataKey
    {
        public byte[] Bytes;

        public DataKey()
        {
            Bytes = CryptManager.GenPacketKey();
        }
        public Packets DecodePacketId(double packetId)
        {
            return (Packets)((double)((double)(packetId * Bytes[1]) / Bytes[0]));
        }
        public bool EqualsTo(byte[] bytes)
        {
            return (bytes[0] == Bytes[1] && bytes[1] == Bytes[0]);
        }
        public void Next()
        {
            Bytes = CryptManager.GenPacketKey();
        }
        public double EncryptPacketId(Packets value)
        {
            return ((double)((double)((short)(value) * Bytes[0]) / Bytes[1]));
        }
    }
}
