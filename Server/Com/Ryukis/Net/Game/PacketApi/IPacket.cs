﻿using EdgeCore.Com.Ryukis.Utils.Buffers;

namespace EdgeCore.Com.Ryukis.Net.Game.PacketApi
{
    interface IPacket
    {
        bool Decode();
        BytesWriter Encode();
        Session Session
        {
            get;
            set;
        }
        Packets PacketId
        {
            get;
        }
        bool CanDestroy
        {
            get;
        }
        BytesWriter Writer
        {
            get;
            set;
        }
        BytesReader Reader
        {
            get;
            set;
        }
    }
}
