﻿using EdgeCore.Com.Ryukis.Managers.Logs;
using EdgeCore.Com.Ryukis.Net.Game.Data;
using EdgeCore.Com.Ryukis.Utils.Buffers;
using System;
using System.Collections.Generic;

namespace EdgeCore.Com.Ryukis.Net.Game.PacketApi
{
    enum DataType : byte
    {
        BYTE = 0,
        SHORT = 1,
        INT = 2,
        FLOAT = 3,
        STRING = 4
    }
    class WPacket : IDisposable
    {
        public Packets PacketType;
        private List<DataType> structure = new List<DataType>();
        private List<object> data = new List<object>();

        public BytesWriter Encode(DataKey packetEncoder)
        {
            if (PacketType == null) return null;
            BytesWriter writer = new BytesWriter();
            try
            {
                writer.WriteDouble(packetEncoder.EncryptPacketId(PacketType));
                BytesWriter tmpBuffer = new BytesWriter();
                for (int i = 0; i < structure.Count; i++)
                {
                    switch (structure[i])
                    {
                        case DataType.BYTE:
                            tmpBuffer.WriteByte((byte)data[i]);
                            break;
                        case DataType.SHORT:
                            tmpBuffer.WriteShort((short)data[i]);
                            break;
                        case DataType.INT:
                            tmpBuffer.WriteInt((int)data[i]);
                            break;
                        case DataType.FLOAT:
                            tmpBuffer.WriteFloat((float)data[i]);
                            break;
                        case DataType.STRING:
                            tmpBuffer.WriteUTF((string)data[i]);
                            break;
                    }
                }
                if((short)PacketType > 1000)
                {
                    writer.WriteInt(tmpBuffer.Bytes.Length+2);
                }
                else
                {
                    writer.WriteShort((short)(tmpBuffer.Bytes.Length+2));
                }
                writer.WriteBytes(tmpBuffer.Bytes);
                packetEncoder.Next();
                writer.WriteByte(packetEncoder.Bytes[0]);
                writer.WriteByte(packetEncoder.Bytes[1]);
                tmpBuffer.Clear();
                tmpBuffer = null;
            }
            catch (Exception)
            {
                Logger.GetInstance().GetLogger("GLOBAL").Error("Error on write packet " + PacketType);
            }
            return writer;
        }
        public void Dispose()
        {
            data.Clear();
            data = null;
            structure.Clear();
            structure = null;
        }
        public void WriteByte(byte value)
        {
            structure.Add(DataType.BYTE);
            data.Add(value);
        }
        public void WriteShort(short value)
        {
            structure.Add(DataType.SHORT);
            data.Add(value);
        }
        public void WriteInt(short value)
        {
            structure.Add(DataType.INT);
            data.Add(value);
        }
        public void WriteUTF(string value)
        {
            structure.Add(DataType.STRING);
            data.Add(value);
        }
        public void WriteFloat(float value)
        {
            structure.Add(DataType.FLOAT);
            data.Add(value);
        }
    }
}
