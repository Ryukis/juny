﻿using EdgeCore.Com.Ryukis.Net.Game.PacketApi.Process;
using System;
using System.Collections.Generic;

namespace EdgeCore.Com.Ryukis.Net.Game.PacketApi
{
    public enum Packets : short
    {
        KEY = 0,
        AUTHENTIFICATION = 895,
        CHARACTERS_COUNT_BY_SERVER = 71
    }
    class PacketsCollection
    {
        private static Dictionary<Packets, Type> packetTypesList = new Dictionary<Packets, Type>()
        {
            { Packets.KEY, typeof(KeyBuffer) },
            { Packets.AUTHENTIFICATION, typeof(Authentification) }
        };
        public static IPacket GetPacketById(Packets id, Session session)
        {
            if (packetTypesList.ContainsKey(id))
            {
                session.Logs.Trace("Packet Id " + Enum.GetName(typeof(Packets), id));
                return (IPacket)(Activator.CreateInstance(packetTypesList[id]));
            }
            session.Logs.Trace("Unexpected packet Id " + id);
            return null;
        }
    }
}
