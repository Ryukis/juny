﻿using EdgeCore.Com.Ryukis.Managers;
using EdgeCore.Com.Ryukis.Utils.Buffers;
using System;

namespace EdgeCore.Com.Ryukis.Net.Game.PacketApi.Process
{
    class KeyBuffer : IPacket
    {
        private Session session;
        private BytesReader reader;
        private BytesWriter writer;

        public BytesWriter Encode()
        {
            writer = new BytesWriter();
            writer.WriteByte(session.Data.Key.Bytes[0]);
            writer.WriteByte(session.Data.Key.Bytes[1]);
            return writer;
        }
        public bool Decode()
        {
            try
            {
                return session.Data.Key.EqualsTo(reader.Bytes);
            }
            catch (Exception){}
            return false;
        }
        public bool CanDestroy
        {
            get { return true; }
        }
        public Packets PacketId
        {
            get { return Packets.KEY; }
        }
        public BytesWriter Writer
        {
            get { return Writer; }
            set { writer = value; }
        }
        public BytesReader Reader
        {
            get { return reader; }
            set { reader = value; }
        }
        public Session Session
        {
            get { return session; }
            set { session = value; }
        }
    }
}
