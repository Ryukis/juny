﻿using EdgeCore.Com.Ryukis.Managers.Game;
using EdgeCore.Com.Ryukis.Net.Game.Data;
using EdgeCore.Com.Ryukis.Utils.Buffers;
using System;

namespace EdgeCore.Com.Ryukis.Net.Game.PacketApi.Process
{
    enum AccountValidation : byte
    {
        VALID = 0,
        BANNED = 1,
        INVALID = 2,
        UNAUTHORISED = 3,
        VALID_USERNAME = 4
    }
    class Authentification : IPacket
    {
        private string user;
        private string pass;

        private bool hasData = false;
        private Session session;
        private Account account;
        private BytesReader reader;
        private BytesWriter writer;
        private bool canDestroy = false;

        public BytesWriter Encode()
        {
            writer = new BytesWriter();
            return writer;
        }
        public bool Decode()
        {
            try
            {
                bool modifyUserName = reader.ReadByte() == 0 ? false : true;
                WPacket packet = new WPacket();
                packet.PacketType = Packets.AUTHENTIFICATION;
                if (user == null || modifyUserName)
                {
                    user = reader.ReadUTF();
                    account = AccountsManager.getInstance().GetAccountByUserName(user);
                    if (account == null)
                    {
                        packet.WriteByte((byte)AccountValidation.INVALID);
                    }
                    else
                    {
                        packet.WriteByte((byte)AccountValidation.VALID_USERNAME);
                        packet.WriteUTF(account.Pass.Substring(account.Pass.Length - 22));
                    }
                    session.Send(packet.Encode(session.Data.Key));
                }
                else
                {
                    pass = reader.ReadUTF();
                    session.Logs.Important("Authentificating " + user + "...");
                    if(pass == account.Pass.Substring(0, account.Pass.Length - 22))
                    {
                        if(account.BannedId == 0)
                        {
                            if (account.Rank >= RealmServer.getInstance().MinRankAccess)
                            {
                                packet.WriteByte((byte)AccountValidation.VALID);
                                canDestroy = true;
                            }
                            else packet.WriteByte((byte)AccountValidation.UNAUTHORISED);
                        }
                        else
                        {
                            packet.WriteByte((byte)AccountValidation.BANNED);
                        }
                    }
                    else
                    {
                        packet.WriteByte((byte)AccountValidation.INVALID);
                    }
                    session.Send(packet.Encode(session.Data.Key));
                }
                return true;
            }
            catch (Exception e) { Console.WriteLine(e.StackTrace); }
            return false;
        }
        public bool CanDestroy
        {
            get { return canDestroy; }
        }
        public Packets PacketId
        {
            get { return Packets.AUTHENTIFICATION; }
        }
        public BytesWriter Writer
        {
            get { return Writer; }
            set { writer = value; }
        }
        public BytesReader Reader
        {
            get { return reader; }
            set { reader = value; }
        }
        public bool HasData
        {
            get { return hasData; }
        }
        public Session Session
        {
            get { return session; }
            set { session = value; }
        }
    }
}
