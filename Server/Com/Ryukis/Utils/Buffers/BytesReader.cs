﻿using System;
using System.Text;

namespace EdgeCore.Com.Ryukis.Utils.Buffers
{
    class BytesReader : IDisposable
    {
        public byte[] Bytes;
        public int Position = 0;

        public BytesReader(byte[] Bytes)
        {
            this.Bytes = Bytes;
        }
        public void Dispose()
        {
            Array.Clear(Bytes, 0, Bytes.Length);
            Bytes = null;
        }
        public byte ReadByte()
        {
            Position++;
            return Bytes[Position-1];
        }
        public short ReadShort()
        {
            Array.Reverse(Bytes, Position, 2);
            Position += 2;
            return BitConverter.ToInt16(Bytes, Position - 2);
        }
        public short[] ReadShortArray()
        {
            short countValue = ReadShort();
            short[] final = new short[countValue];
            for (short i = 0; i < countValue; i++)
            {
                final[i] = ReadShort();
            }
            return final;
        }
        public float ReadFloat()
        {
            Array.Reverse(Bytes, Position, 4);
            Position += 4;
            return BitConverter.ToSingle(Bytes, Position - 4);
        }
        public double ReadDouble()
        {
            Array.Reverse(Bytes, Position, 8);
            Position += 8;

            return BitConverter.ToDouble(Bytes, Position - 8);
        }
        public int ReadInt()
        {
            Array.Reverse(Bytes, Position, 4);
            Position += 4;

            if(Position < Bytes.Length - 2)
            {
                return BitConverter.ToInt32(Bytes, Position - 4);
            }
            return 0;
        }
        public int[] ReadIntArray()
        {
            short countValue = ReadShort();
            int[] final = new int[countValue];
            for (short i = 0; i < countValue; i++)
            {
                final[i] = ReadInt();
            }
            return final;
        }
        public string ReadUTF()
        {
            short strSize = ReadShort();
            Position += strSize;
            return new ASCIIEncoding().GetString(Bytes, Position - strSize, strSize);
        }
        public string[] ReadUTFArray()
        {
            short countValue = ReadShort();
            string[] final = new string[countValue];
            for (short i = 0; i < countValue; i++)
            {
                final[i] = ReadUTF();
            }
            return final;
        }
        public byte[] ReadBytes(int count)
        {
            byte[] result = new byte[count];
            Buffer.BlockCopy(Bytes, Position, result, 0, count);
            Position += count;
            return result;
        }
    }
}
