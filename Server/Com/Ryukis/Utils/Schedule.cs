﻿using System;

namespace EdgeCore.Com.Ryukis.Utils
{
    class Schedule
    {
        public int milliseconds;
        public int scheduleId;
        public long endDate;
        public Action method;

        public Schedule(int scheduleId, Action method, int milliseconds)
        {
            this.scheduleId = scheduleId;
            this.milliseconds = milliseconds;
            this.method = method;
            endDate = (DateTime.Now.Ticks + (TimeSpan.TicksPerMillisecond * milliseconds));
        }
        public Schedule clone()
        {
            return new Schedule(scheduleId, method, milliseconds);
        }
        public Schedule refresh()
        {
            endDate = (DateTime.Now.Ticks + (TimeSpan.TicksPerMillisecond * milliseconds));
            return this;
        }
    }
}
