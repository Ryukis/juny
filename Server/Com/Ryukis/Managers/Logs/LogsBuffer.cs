﻿using System;
using System.Collections.Generic;
using System.IO;

namespace EdgeCore.Com.Ryukis.Managers.Logs
{
    class LogsBuffer
    {
        private List<string> bufferingString = new List<string>();
        public string LogsPath;
        public string Name;

        public LogsBuffer(string name)
        {
            short i = 0;
            string genericPath = "logs/" + DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + "/" + name + "/" + i + "/";
            while (Directory.Exists(genericPath))
            {
                i++;
                genericPath = "logs/" + DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + "/" + name + "/" + i + "/";
            }
            LogsPath = genericPath + "logs.txt";
            Name = name;
            Directory.CreateDirectory(genericPath);
        }
        public void Trace(string tr)
        {
            tr = "[" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + "," + DateTime.Now.Second + "] " + Name + " : " + tr;
            bufferingString.Add(tr);
            Console.WriteLine(tr);
            if (bufferingString.Count > 100)
                Flush();
        }
        public void Important(string im)
        {
            im = "[" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + "," + DateTime.Now.Second + "] " + Name + " : " + im;
            bufferingString.Add(im);
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine(im);
            Console.ResetColor();
            if (bufferingString.Count > 100)
                Flush();
        }
        public void Info(string inf)
        {
            inf = "[" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + "," + DateTime.Now.Second + "] " + Name + " : " + inf;
            bufferingString.Add(inf);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(inf);
            Console.ResetColor();
            if (bufferingString.Count > 100)
                Flush();
        }
        public void Error(string er)
        {
            er = "[" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + "," + DateTime.Now.Second + "] " + Name + " : " + er;
            bufferingString.Add(er);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(er);
            Console.ResetColor();
            if (bufferingString.Count > 100)
                Flush();
        }
        public void Flush()
        {
            return;
            string final = "";
            while(bufferingString.Count > 0)
            {
                final += bufferingString[0] + "\n";
                bufferingString.RemoveAt(0);
            }
            File.AppendAllText(LogsPath, final);
        }
    }
}
