﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace EdgeCore.Com.Ryukis.Managers.Logs
{
    class Logger
    {
        private static Logger _self;
        private static Dictionary<string, LogsBuffer> buffers;
        private System.Timers.Timer flushTimer = new System.Timers.Timer(10000);

        public void Start()
        {
            _self = this;
            buffers = new Dictionary<string, LogsBuffer>();
            flushTimer.Elapsed += OnFlushTimerElapsed;
            flushTimer.AutoReset = false;
            if (!Directory.Exists("logs/")) Directory.CreateDirectory("logs/");
        }
        public void FlushAll()
        {
            foreach(KeyValuePair<string, LogsBuffer> key in buffers)
            {
                key.Value.Flush();
            }
            buffers.Clear();
            Thread.Sleep(1);
        }
        public LogsBuffer GetLogger(string name)
        {
            flushTimer.Stop();
            flushTimer.Start();
            if(buffers.ContainsKey(name))
                return buffers[name];
            else
            {
                buffers.Add(name, new LogsBuffer(name));
                return buffers[name];
            }
        }
        private void OnFlushTimerElapsed(object sender, EventArgs e)
        {
            FlushAll();
        }
        public static Logger GetInstance()
        {
            return _self;
        }
    }
}
