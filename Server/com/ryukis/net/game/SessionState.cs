﻿namespace EdgeCore.Com.Ryukis.Net.Game
{
    public enum SessionState
    {
        NEW = 0,
        CERTIFICATION = 1,
        WAITING_FULL_PACKET = 2,
        WAITING_MEDIUM_PACKET = 3,
        WAITING_SMALL_PACKET = 4,
        WAITING_PACKET_TYPE = 5
    }
}
