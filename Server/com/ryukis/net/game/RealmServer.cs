﻿using EdgeCore.Com.Ryukis.Managers;
using EdgeCore.Com.Ryukis.Managers.Logs;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace EdgeCore.Com.Ryukis.Net.Game
{
    class RealmServer
    {
        public byte MinRankAccess = 0;
        private static RealmServer _self;
        private ManualResetEvent onConnect = new ManualResetEvent(false);
        private List<Session> sessionList;
        private int connectionId = int.MinValue;
        private Socket socketListener;
        private IPEndPoint iep;

        public RealmServer()
        {
            iep = new IPEndPoint(IPAddress.Parse(Config.Host), Config.Port);
        }
        public void RemoveSession(Session session)
        {
            for(int i = 0;i < sessionList.Count; i++)
            {
                if(sessionList[i] == session)
                {
                    object sessionTrashed = sessionList[i];
                    TrashManager.getInstance().addToTrash(ref sessionTrashed);
                    sessionList.RemoveAt(i);
                    return;
                }
            }
        }
        public void Init(bool isReboot=false)
        {
            if (!isReboot)
            {
                socketListener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                sessionList = new List<Session>();
            }
            else
            {
                while(sessionList.Count > 0)
                {
                    if(sessionList[0] != null)
                        sessionList[0].Close();
                    sessionList.RemoveAt(0);
                }
            }
            socketListener.Bind(iep);
            socketListener.Listen(10000);
        }
        public void Start()
        {
            socketListener.Blocking = false;
            socketListener.BeginAccept(new AsyncCallback(accept), socketListener);
            Logger.GetInstance().GetLogger("GLOBAL").Info("Edge Core listening connections on port " + Config.Port);
            onConnect.WaitOne();
        }
        private void accept(IAsyncResult sock)
        {
            Socket newListener = (Socket)sock.AsyncState;
            Session session = new Session(socketListener.EndAccept(sock));
            socketListener = newListener;
            connectionId++;
            session.init();
            sessionList.Add(session);
            socketListener.BeginAccept(accept, socketListener);
        }
        public void Reboot()
        {
            Init(true);
        }
        public static RealmServer getInstance()
        {
            return (_self == null ? _self = new RealmServer() : _self);
        }
    }
}
