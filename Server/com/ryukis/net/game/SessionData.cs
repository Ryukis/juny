﻿using EdgeCore.Com.Ryukis.Net.Game.Data;

namespace EdgeCore.Com.Ryukis.Net.Game
{
    class SessionData
    {
        public DataKey Key;
        public Account Account;
    }
}
