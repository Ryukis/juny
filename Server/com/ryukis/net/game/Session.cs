﻿using EdgeCore.Com.Ryukis.Managers.Logs;
using EdgeCore.Com.Ryukis.Net.Game.Data;
using EdgeCore.Com.Ryukis.Net.Game.PacketApi;
using EdgeCore.Com.Ryukis.Utils.Buffers;
using System;
using System.Net;
using System.Net.Sockets;

namespace EdgeCore.Com.Ryukis.Net.Game
{
    class Session
    {
        public readonly SessionData Data;
        private Socket socket;
        private SessionState state = SessionState.NEW;
        private IPacket bufferingPacket;
        private int tmpPacketSize;
        public LogsBuffer Logs;
        public string Ip;

        public Session(Socket socket)
        {
            Data = new SessionData();
            this.socket = socket;
        }
        public bool isConnected
        {
            get {
                if (socket == null) return false;
                return !(socket.Poll(5000, SelectMode.SelectRead) && socket.Available == 0);
            }
        }
        public void init()
        {
            Ip = (socket.RemoteEndPoint as IPEndPoint).Address.ToString();
            Logger.GetInstance().GetLogger(Ip).Important("New Connection opened");
            Logs = Logger.GetInstance().GetLogger(Ip);
            bufferingPacket = PacketsCollection.GetPacketById(Packets.KEY, this);
            bufferingPacket.Session = this;
            Data.Key = new DataKey();
            prepareReceive();
            Send(bufferingPacket.Encode());
        }
        public void Close()
        {
            try
            {
                Logs.Important("Connection closed by server");
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
                RealmServer.getInstance().RemoveSession(this);
            }
            catch (Exception) { }
        }
        private void prepareReceive()
        {
            try
            {
                byte[] data;
                switch (state)
                {
                    case SessionState.WAITING_PACKET_TYPE:
                        data = new byte[8];
                        socket.BeginReceive(data, 0, 8, 0, OnPacketType, data);
                        break;
                    case SessionState.WAITING_SMALL_PACKET:
                        data = new byte[2];
                        socket.BeginReceive(data, 0, 2, 0, onSmallPacketSize, data);
                        break;
                    case SessionState.WAITING_MEDIUM_PACKET:
                        data = new byte[4];
                        socket.BeginReceive(data, 0, 4, 0, onMediumPacketSize, data);
                        break;
                    case SessionState.WAITING_FULL_PACKET:
                        data = new byte[tmpPacketSize];
                        socket.BeginReceive(data, 0, tmpPacketSize, 0, OnFullPacket, data);
                        break;
                    case SessionState.NEW:
                        data = new byte[2];
                        socket.BeginReceive(data, 0, 2, 0, OnKeyBuffer, data);
                        break;
                }
            }
            catch (Exception) { Close(); }
        }
        private void OnFullPacket(IAsyncResult iar)
        {
            if(bufferingPacket == null)
            {
                Close();
                return;
            }
            Logs.Trace("<< " + bufferingPacket.PacketId + " :: " + ((byte[])iar.AsyncState).Length + " bytes received");
            bufferingPacket.Session = this;
            bufferingPacket.Reader = new BytesReader((byte[])iar.AsyncState);
            if (bufferingPacket.Decode())
            {
                if (bufferingPacket.CanDestroy)
                {
                    bufferingPacket = null;
                    state = SessionState.WAITING_PACKET_TYPE;
                }
                else state = (short)bufferingPacket.PacketId > 1000 ? SessionState.WAITING_MEDIUM_PACKET : SessionState.WAITING_SMALL_PACKET;
                prepareReceive();
            }
            else
            {
                Close();
            }
        }
        private void onMediumPacketSize(IAsyncResult iar)
        {
            try
            {
                using (BytesReader reader = new BytesReader((byte[])iar.AsyncState))
                {
                    tmpPacketSize = reader.ReadInt();
                    state = SessionState.WAITING_FULL_PACKET;
                }
                prepareReceive();
            }
            catch (Exception) { Close(); }
        }
        private void onSmallPacketSize(IAsyncResult iar)
        {
            try
            {
                using (BytesReader reader = new BytesReader((byte[])iar.AsyncState))
                {
                    tmpPacketSize = reader.ReadShort();
                    state = SessionState.WAITING_FULL_PACKET;
                }
                prepareReceive();
            }
            catch (Exception) { Close(); }
        }
        private void OnPacketType(IAsyncResult iar)
        {
            try
            {
                using (BytesReader reader = new BytesReader((byte[])iar.AsyncState))
                {
                    bufferingPacket = PacketsCollection.GetPacketById(Data.Key.DecodePacketId(reader.ReadDouble()), this);
                    if(bufferingPacket == null)
                    {
                        Close();
                        return;
                    }
                    if((short)bufferingPacket.PacketId > 1000)
                    {
                        state = SessionState.WAITING_MEDIUM_PACKET;
                    }
                    else
                    {
                        state = SessionState.WAITING_SMALL_PACKET;
                    }
                    prepareReceive();
                }
            }
            catch (Exception)
            {
                Close();
            }
        }
        private void OnKeyBuffer(IAsyncResult iar)
        {
            if(bufferingPacket == null)
            {
                Close();
                return;
            }
            bufferingPacket.Session = this;
            bufferingPacket.Reader = new BytesReader((byte[])iar.AsyncState);

            if (bufferingPacket.Decode())
            {
                state = SessionState.WAITING_SMALL_PACKET;
                bufferingPacket = PacketsCollection.GetPacketById(Packets.AUTHENTIFICATION, this);
                Logs.Info("Connection etablished safely");
                prepareReceive();
            }
            else
            {
                Logs.Error("Fake Connection detected ! Kicking...");
                Close();
            }
        }

        #region SendData
        private void SendCallBack(IAsyncResult iar) { }

        public void Send(BytesWriter buffer)
        {
            try
            {
                Logs.Trace(" >> " + buffer.BufferSize + " bytes [" + buffer.ToHexString() + "]");
                socket.BeginSend(buffer.Bytes, 0, buffer.BufferSize, 0, new AsyncCallback(SendCallBack), socket);
            }
            catch (Exception) {Close(); }
        }
        #endregion
    }
}
