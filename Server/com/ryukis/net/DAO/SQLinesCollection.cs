﻿using System.Collections.Generic;

namespace EdgeCore.Com.Ryukis.Net.DAO.preparedStatements
{
    public enum CommandList : short
    {
        SEARCH_ACCOUNT_BY_USERNAME = 0
    }
    class SQLinesCollection
    {
        private static Dictionary<CommandList, string> commandList = new Dictionary<CommandList, string>()
        {
            {CommandList.SEARCH_ACCOUNT_BY_USERNAME, "SELECT * FROM `accounts` WHERE `account` = @userName" }
        };
        public static bool IsValidCommand(CommandList command, string commandText)
        {
            return commandList.TryGetValue(command, out commandText);
        }
        public static string GetCommand(CommandList command)
        {
            return commandList[command];
        }
    }
}
