﻿using EdgeCore.Com.Ryukis.Managers.Logs;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace EdgeCore.Com.Ryukis.Net.DAO.Api
{
    class DAOReader : Dictionary<string, object>
    {
        private LogsBuffer Logs;
        public DAOReader(MySqlDataReader Reader)
        {
            Logs = Logger.GetInstance().GetLogger("DAOReader");
            Reader.Read();
            if (Reader.HasRows) for(short i = 0;i < Reader.FieldCount; i++) this[Reader.GetName(i)] = Reader.GetValue(i);
        }
        public int GetInt(string key)
        {
            try { return (int)this[key]; }
            catch (Exception) { Logs.Error("Unable to read int with key : " + key); }
            return 0;
        }
        public string GetString(string key)
        {
            try { return (string)this[key]; }
            catch (Exception) { Logs.Error("Unable to read string with key : " + key); }
            return "";
        }
        public DateTime GetDateTime(string key)
        {
            try { return (DateTime)this[key]; }
            catch (Exception) { Logs.Error("Unable to read DateTime with key : " + key); }
            return DateTime.Now;
        }
        public byte GetByte(string key)
        {
            try { return (byte)this[key]; }
            catch (Exception) { Logs.Error("Unable to read byte with key : " + key); }
            return 0;
        }
        public short GetShort(string key)
        {
            try
            {
                return (short)this[key];
            }
            catch (Exception)
            {
                Logs.Error("Unable to read short with key : " + key);
            }
            return 0;
        }
    }
}
