﻿using MySql.Data.MySqlClient;
using System;

namespace EdgeCore.Com.Ryukis.Net.DAO.Api
{
    class ExecutedCommand
    {
        public DAOReader Reader;

        public ExecutedCommand(MySqlDataReader Reader, MySqlCommand Command, MySqlConnection Connection)
        {
            this.Reader = new DAOReader(Reader);
            Reader.Close();
            Reader.Dispose();
            Reader = null;
            Command.Dispose();
            Connection.Close();
            Connection.Dispose();
            Connection = null;
            Command = null;
        }
    }
}
