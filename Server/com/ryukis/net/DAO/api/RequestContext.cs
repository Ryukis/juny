﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace EdgeCore.Com.Ryukis.Net.DAO.Api
{
    class RequestContext
    {
        public MySqlConnection Connection;
        public MySqlCommand Command;
        public MySqlDataReader Reader;
        public string PreparedStatement;
        public Dictionary<string, string> Parameters;

        public RequestContext(MySqlConnection conn)
        {
            Connection = conn;
        }
        public ExecutedCommand SelectRequest(string sqline, Dictionary<string, string> parameters=null)
        {
            if (parameters != null) Parameters = parameters;
            PreparedStatement = sqline;
            if(Parameters != null && Parameters.Count > 0) PrepareParameters();
            Connection.Open();
            Command = new MySqlCommand(PreparedStatement, Connection);
            return new ExecutedCommand(Command.ExecuteReader(), Command, Connection);
        }
        private void PrepareParameters()
        {
            if (PreparedStatement.Contains("@"))
            {
                foreach (string key in Parameters.Keys) if (PreparedStatement.Contains("@" + key)) PreparedStatement = PreparedStatement.Replace("@" + key, "'" + Parameters[key] + "'");
                PreparedStatement = PreparedStatement.Replace("''", "'");
                if (!PreparedStatement.EndsWith(";")) PreparedStatement += ";";
            }
        }
    }
}
