﻿using EdgeCore.Com.Ryukis.Net.DAO.Api;
using EdgeCore.Com.Ryukis.Net.DAO.preparedStatements;
using EdgeCore.Com.Ryukis.Net.Game.Data;
using System.Collections.Generic;

namespace EdgeCore.Com.Ryukis.Net.DAO.Entities
{
    class AccountApi
    {
        public static Account loadAccount(string userName)
        {
            Dictionary<string, string> accParameters = new Dictionary<string, string>()
            {
                {"userName", userName }
            };
            DAOReader Reader = Database.GetInstance().CreateRequest().SelectRequest(SQLinesCollection.GetCommand(CommandList.SEARCH_ACCOUNT_BY_USERNAME), accParameters).Reader;
            Account acc = new Account();
            acc.BannedId = Reader.GetShort("bannedId");
            acc.Guid = Reader.GetInt("guid");
            acc.Pass = Reader.GetString("pass");
            acc.Gender = Reader.GetByte("gender");
            acc.UserName = Reader.GetString("userName");
            acc.Birthday = Reader.GetDateTime("birthday");
            acc.LastConnectionDate = Reader.GetDateTime("lastConnectionDate");
            acc.LastIp = Reader.GetString("lastIp");
            acc.Rank = Reader.GetByte("rank");
            acc.CreationDate = Reader.GetDateTime("creationDate");
            acc.CreationIp = Reader.GetString("creationIp");
            acc.Characters = new KeyValuePair<byte, int[]>[Config.GameServersCount];

            string characters = Reader.GetString("characters");

            if (characters.Length > 0)
            {
                string[] servers = characters.Split('|');

                for (byte i = 0; i < servers.Length; i++)
                {
                    string[] values = servers[i].Split(';');
                    int[] characterIds = new int[values.Length - 1];
                    for (byte j = 1; j < values.Length; j++)
                    {
                        characterIds[j - 1] = int.Parse(values[j]);
                    }
                    acc.Characters[i] = new KeyValuePair<byte, int[]>(byte.Parse(values[0]), characterIds);
                }
            }
            return acc;
        }
    }
}
