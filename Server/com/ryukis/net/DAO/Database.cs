﻿using EdgeCore.Com.Ryukis.Managers.Logs;
using EdgeCore.Com.Ryukis.Net.DAO.Api;
using MySql.Data.MySqlClient;
using System;

namespace EdgeCore.Com.Ryukis.Net.DAO
{
    class Database
    {
        private static Database _self;
        private string connString;

        public Database()
        {
            connString = "SERVER=" + Config.DbHost + ";Database=" + Config.DbName + ";Uid=" + Config.DbUser + ";PWD=" + Config.DbPass+";";
        }
        public RequestContext CreateRequest()
        {
            try
            {
                return new RequestContext(new MySqlConnection(connString));
            }
            catch (Exception)
            {
                Logger.GetInstance().GetLogger("GLOBAL").Error("Connection to database failed");
                return null;
            }
        }
        public static Database GetInstance()
        {
            return (_self == null ? _self = new Database() : _self);
        }
    }
}
