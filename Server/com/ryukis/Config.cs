﻿namespace EdgeCore.Com.Ryukis
{
    class Config
    {
        #region ServerHost
        public static string Host;
        public static int Port;
        #endregion

        #region Database
        public static string DbHost;
        public static string DbUser;
        public static string DbPass;
        public static string DbName;
        #endregion

        #region GameServers
        public static byte GameServersCount = 1;
        #endregion
    }
}
