﻿using System;
using System.Text;

namespace EdgeCore.Com.Ryukis.Managers
{
    class CryptManager
    {
        private static byte[] alphabetToBytes = Encoding.ASCII.GetBytes(new char[36] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' });

        public static string GenSalt()
        {
            return BCrypt.Net.BCrypt.GenerateSalt(10);
        }
        public static byte[] GenPacketKey()
        {
            Random rdm = new Random();
            return new byte[2] { (byte)rdm.Next(0, 127), (byte)rdm.Next(128, 255) } ;
        }
    }
}
