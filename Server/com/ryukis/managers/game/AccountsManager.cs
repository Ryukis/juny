﻿using EdgeCore.Com.Ryukis.Net.DAO.Entities;
using EdgeCore.Com.Ryukis.Net.Game.Data;
using System.Collections.Generic;

namespace EdgeCore.Com.Ryukis.Managers.Game
{
    class AccountsManager
    {
        private static AccountsManager _self;
        private Dictionary<string, Account> accountList = new Dictionary<string, Account>();

        public AccountsManager()
        {

        }
        public Account GetAccountByUserName(string userName)
        {
            if (accountList.ContainsKey(userName)) return accountList[userName];
            return AccountApi.loadAccount(userName);
        }
        public void AddAccount(Account account)
        {
            if (!accountList.ContainsKey(account.UserName)) accountList.Add(account.UserName, account);
        }
        public static AccountsManager getInstance()
        {
            return (_self == null ? _self = new AccountsManager() : _self);
        }
    }
}
