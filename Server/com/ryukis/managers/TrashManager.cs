﻿using EdgeCore.Com.Ryukis.Managers.Logs;
using EdgeCore.Com.Ryukis.Utils;
using System.Collections.Generic;

namespace EdgeCore.Com.Ryukis.Managers
{
    class TrashManager
    {
        private static TrashManager _self;
        private Schedule cleanTask;
        private bool cleanRunning = false;
        private List<object> trash = new List<object>();

        public TrashManager()
        {
            cleanTask = new Schedule(ScheduleManager.getId(), onCleanTask, 15000);
        }
        public void addToTrash(ref object element)
        {
            trash.Add(element);

            if (!cleanRunning)
            {
                ScheduleManager.getInstance().addSchedule(cleanTask);
                cleanRunning = true;
            }
        }
        private void onCleanTask()
        {
            while(trash.Count > 0)
            {
                trash[0] = null;
                trash.RemoveAt(0);
            }
            cleanRunning = false;
            Logger.GetInstance().GetLogger("GLOBAL").Trace("Clean task finished");
        }
        public static TrashManager getInstance()
        {
            return (_self == null ? _self = new TrashManager() : _self);
        }
    }
}
