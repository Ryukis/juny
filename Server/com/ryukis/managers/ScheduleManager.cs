﻿using EdgeCore.Com.Ryukis.Utils;
using System;
using System.Collections.Generic;
using System.Timers;

namespace EdgeCore.Com.Ryukis.Managers
{
    class ScheduleManager
    {
        private static ScheduleManager _self;
        private static int lastScheduleId = int.MinValue;
        private Timer timer;
        private List<Schedule> nextSchedules = new List<Schedule>();
        private List<Schedule> scheduleList = new List<Schedule>();

        public ScheduleManager()
        {
            timer = new Timer();
            timer.Elapsed += onTimerElapsed;
        }
        public void removeSchedule(int scheduleId)
        {
            for(int i = 0;i < nextSchedules.Count; i++)
            {
                if(nextSchedules[i].scheduleId == scheduleId)
                {
                    nextSchedules[i] = null;
                    nextSchedules.RemoveAt(i);
                    if(nextSchedules.Count == 0)
                    {
                        initNextSchedule();
                    }
                    return;
                }
            }
            for(int i = 0;i < scheduleList.Count; i++)
            {
                if(scheduleList[i].scheduleId == scheduleId)
                {
                    scheduleList[i] = null;
                    scheduleList.RemoveAt(i);
                    return;
                }
            }
        }
        public void addSchedule(Schedule schedule)
        {
            if(nextSchedules.Count > 0)
            {
                if(nextSchedules[0].endDate > schedule.endDate)
                {
                    timer.Stop();
                    while(nextSchedules.Count > 0)
                    {
                        scheduleList.Add(nextSchedules[0]);
                        nextSchedules.RemoveAt(0);
                    }
                    initNextSchedule();
                }
                else
                {
                    scheduleList.Add(schedule);
                }
            }
            else
            {
                nextSchedules.Add(schedule);
                start();
            }
        }
        private void initNextSchedule()
        {
            long smallerEndDate = long.MaxValue;
            for(int i = 0;i < scheduleList.Count; i++)
            {
                if(smallerEndDate > scheduleList[i].endDate+50) smallerEndDate = scheduleList[i].endDate;
            }
            for(int i = 0;i < scheduleList.Count; i++)
            {
                if (smallerEndDate >= scheduleList[i].endDate - 50 || smallerEndDate <= scheduleList[i].endDate + 50)
                {
                    nextSchedules.Add(scheduleList[i]);
                    scheduleList.RemoveAt(i);
                    i--;
                }
            }
            if (nextSchedules.Count > 0) start();
        }
        private void start()
        {
            if(nextSchedules != null && (nextSchedules[0].endDate - DateTime.Now.Ticks) > 0)
            {
                timer.Interval = (nextSchedules[0].endDate - DateTime.Now.Ticks) / TimeSpan.TicksPerMillisecond;
                timer.Start();
            }
        }
        private void onTimerElapsed(object sender, EventArgs e)
        {
            while(nextSchedules.Count > 0)
            {
                nextSchedules[0].method();
                nextSchedules.RemoveAt(0);
            }
            initNextSchedule();
        }
        public static int getId()
        {
            if (lastScheduleId == int.MaxValue) lastScheduleId = int.MinValue;
            return ++lastScheduleId;
        }
        public static ScheduleManager getInstance()
        {
            return (_self == null ? _self = new ScheduleManager() : _self);
        }
    }
}
