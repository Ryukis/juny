﻿using EdgeCore.Com.Ryukis;
using EdgeCore.Com.Ryukis.Managers.Logs;
using EdgeCore.Com.Ryukis.Net.Game;
using System;
using System.Threading;

namespace EdgeCore
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Starting Edge Core. Please wait...\nInitializing...");
            new Thread(new Logger().Start).Start();
            Thread.Sleep(100);

            Config.Port = 50555;
            Config.Host = "0.0.0.0";
            Config.DbHost = "127.0.0.1";
            Config.DbUser = "root";
            Config.DbPass = "";
            Config.DbName = "realm_edge_core";

            RealmServer.getInstance().Init();
            Console.WriteLine("--------------------------------------------------------------------------");
            RealmServer.getInstance().Start();
        }
    }
}
